(ns poe-ranknator.core
  (:gen-class)
  (:require [org.httpkit.server :as server]
            [poe-ranknator
             [html :as html]
             [ranker :as ranker]]))

(def names ["OrBaruk" "vr3b3l" "nikiminha"])
(def s2 {"4thReich" {:rank 3656 :inc false} , "ventti666" {:rank 3717 :inc false}, "kombajnova" {:rank 1234 :inc true}})

(defonce myserver (atom nil))
(defonce rankings (ref nil))

(defonce results (ref {}))
(defn app [req]
  {:status  200
   :headers {"Content-Type" "text/html"}
   :body    (html/generate-html (sort-by #((val %) :rank) @results))})

(defn stop-server []
  (when-not (nil? @myserver)
    (@myserver :timeout 100)
    (reset! myserver nil)))

(defn -main
  [& args]
  (reset! myserver (server/run-server #'app {:port 8080}))
  (while true
    (do
      (dosync
       (ref-set rankings (ranker/get-rankings 0 15000 "Hardcore%20Breach"))
       ;; (ref-set rankings (ranker/random-rankings names))
       (ref-set results (ranker/update-results @results @rankings names)))
      (Thread/sleep 60000))))
