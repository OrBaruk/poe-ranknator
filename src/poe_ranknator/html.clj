(ns poe-ranknator.html
  (:require [hiccup.core :refer [html]]))

(def ^:private red-style {:style "margin: 6px 0;color:rgb(246,49,40)"})
(def ^:private green-style {:style "margin: 6px 0;color:rgb(130,177,30)"})
(def ^:private meta-tags {:http-equiv "refresh" :content "30; URL=http://127.0.0.1:8080"})
(def ^:private body-style {:style "background-image:url(https://web.poecdn.com/image/layout/atlas-bg.jpg?1470996538"})
(def ^:private h1-style {:style "margin-bottom:3px;color:#ccc;font-size:120px"})
(def ^:private left-div-style {:style "text-align:left;display:inline-block;padding-left:36px;font-size:100px" })
(def ^:private right-div-style {:style "text-align:right;display:inline-block;padding-left:36px;font-size:100px"})

(defn- get-style
  [b]
  (if b
    red-style
    green-style))

(defn generate-html
  [results]
  (html [:html
         [:head
          [:title "PoE - Ranknator!"]
          [:meta meta-tags]]
         [:body  body-style
          [:h1 h1-style "Ranknator:"]
          [:div left-div-style
           (for [[n v] results]
             [:h2 (get-style (v :inc)) n])
           ]
          [:div right-div-style
           (for [[n v] results]
             [:h2 (get-style (v :inc)) (v :rank)])
           ]]]))
