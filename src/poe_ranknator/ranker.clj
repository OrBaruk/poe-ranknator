(ns poe-ranknator.ranker
  (:require [clojure.data.json :as json]
            [org.httpkit.client :as client]))

(defn request-entries
  [url]
  (Thread/sleep 50) ;bee nice
  ((json/read-str (@(client/get url) :body)) "entries"))

(defn parse-entries
  [entries]
  (reduce #(into %1 [[(%2 "rank") ((%2 "account") "name")]]) {} entries ))

(defn get-rankings
  [start end league]
  (->> (range start end 200)
       (map #(str "http://api.pathofexile.com/ladders/" league "?offset=" % "&limit=200"))
       (reduce #(into %1 (parse-entries (request-entries %2))) {})))

(defn names?
  [value names]
  (reduce #(or %1 (= value %2)) false names))

(defn filter-rankings
  [rankings n]
  (into {} (map #(into [] (reverse %)) (reverse (sort (filter #(names? (val %) n) rankings))))))

(defn random-rankings
  "docstring!!!"
  [n]
  (reduce #(into %1 [[(rand-int 15000) %2]]) {} n))

(defn get-results
  [ranks]
  (reduce (fn [acc [k v]] (assoc acc k {:rank v :inc false} )) {} ranks))

(defn update-result
  [m [k v]]
  (if (nil? (m k))
    (assoc m k {:rank v :inc false})
    (let [old-rank ((m k) :rank)]
      (if (< old-rank v)
        (assoc m k {:rank v :inc true})
        (assoc m k {:rank v :int false})))))

(defn update-results
  [results rankings names]
  (reduce update-result results (filter-rankings rankings names)))
